let topTenGames;
const headers = {
  'user-key': '57d88431d1dfd29a26b29a89cf2c8550',
  Accept: 'application/json'
};

axios
  .get(
    `${'https://cors-anywhere.herokuapp.com/'}https://api-endpoint.igdb.com/games/?fields=name,popularity&order=popularity:desc`,
    { headers }
  )
  .then(response => {
    topTenGamesIds = response.data.map(game => game.id);
    console.log(topTenGamesIds);
    return axios
      .get(
        `${'https://cors-anywhere.herokuapp.com/'}https://api-endpoint.igdb.com/games/${topTenGamesIds}`,
        { headers }
      )
      .then(response => {
        topTenGames = response.data;
        console.log(topTenGames);
        topTenGames.forEach(function(game) {
          createCard(game);
        });
      });
  })
  .catch(e => {
    console.log('error', e);
  });

function createCard(item) {
  //divs
  var mainDiv = document.createElement('div');
  var innerDiv = document.createElement('div');
  var frontDiv = document.createElement('div');
  var backDiv = document.createElement('div');
  var span = document.createElement('span');
  //content
  var badge = document.createTextNode(0);
  var image = document.createElement('img');
  var thumbnail = item.cover.url;
  var cover = thumbnail.replace('thumb', 'cover_big'); // increasing img quality
  image.src = cover;
  var cardTitle = document.createElement('h4');
  var title = document.createTextNode(`${item.name}`);
  var parag = document.createElement('p');
  var paragText = document.createTextNode(
    `${item.summary ? item.summary : "This game doesn't have a summary."}`
  );
  //class defs
  mainDiv.className = 'flip-card';
  innerDiv.className = 'flip-card-inner';
  frontDiv.className = 'flip-card-front';
  backDiv.className = 'flip-card-back';
  //appending
  span.appendChild(badge);
  frontDiv.appendChild(image);
  innerDiv.appendChild(frontDiv);
  cardTitle.appendChild(title);
  backDiv.appendChild(cardTitle);
  parag.appendChild(paragText);
  backDiv.appendChild(parag);
  innerDiv.appendChild(backDiv);
  mainDiv.appendChild(innerDiv);
  mainDiv.appendChild(span);
  document.getElementById('gamesList').appendChild(mainDiv);
  //functionality
  mainDiv.addEventListener('click', function() {
    console.log(`you selected ${item.name}`);
    incrVote();
  });

  function incrVote() {
    badge.nodeValue = Number(badge.nodeValue) + 1;
  }
}
