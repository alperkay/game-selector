# N8<sup>2</sup>

## Objective

We have a Nintendo 64 console at Bloqhouse. Awesome right?! But, as a team, we need to decide which game to buy next.
We currently only have Mario Kart. Create a small single-page website that allows us to vote on our favorite Nintendo
64 game.

## User story

As a Bloqhouse employee handling our finance, I want to buy a new N64 game to which the most people are interested. In
our weekly team meeting, I put my laptop down, connect to the HDTV via HDMI, open the voting website full-screen and
ask all employees one-by-one which game they like. They have to pick from the games shown on the screen. I cast the
votes by clicking on the games.

## Requirements

- It should show the 10-20 most popular N64 games.
  - You can hardcode those games or use an API.
  - If you're a backend developer, we're kind of expecting you to use an API to fetch games.
  - The definition of 'popular' is up to you. Can be rating, sales our your personal opinion.
  - Tip: check out [IGDB API](https://igdb.github.io/api/).
  - Tip: check out [businessinsider.nl/best-nintendo-64-games-ranked-2016-9](https://www.businessinsider.nl/best-nintendo-64-games-ranked-2016-9)
- Show the box arts or game illustrations in a grid, all at once on the page (so no pagination).
- Clicking/tapping on a game casts a vote. The vote count should be visible somehow.
  - Tip: use iOS-esque app badge to show vote count.
- Desktop variant of the webpage.
- **The code should represent you.**

### Out of scope

- Database storage.
- Continuous integration.
- Login/authentication/authorization/ACL.

## Technical requirements

- A backend framework you prefer or not use any framework at all.
- The Vue JS framework, or no JS framework at all. No jQuery.
- The Bootstrap 4 frontend framework, or no frontend framework at all.
- Valid HTML and SCSS.
- Git version control.
- Support Safari `>= 10`, Chrome `>= 65`.
  - Yes, you can ignore your favorite browsers Internet Explorer and Edge :)

### Nice-to-haves

Really, only if you have time left.

- Anything you can think of that would improve the application.
- Make it pretty, add eye-candy, add animations, etc. We like it visual.
- A readme file in Markdown with build instructions and code conventions.
- Mobile and desktop variant. Mobile first SCSS approach.
- Unit and/or end-to-end tests.
- [BEM](http://getbem.com) methodology.
- Linters.

## Inspiration

- The card components from [klart.io/pixels](https://klart.io/pixels)
- The masonry grid from [anothersomething.org/](http://www.anothersomething.org)
- The card hover effect from [keyvalues.com](https://www.keyvalues.com)
- The design off [analogue.co/pages/super-nt/](https://www.analogue.co/pages/super-nt/)
- The design of [universiteitvannederland.nl](https://universiteitvannederland.nl/college)
- Nintendo illustrations and designs at [dribbble.com/search?q=nintendo](https://dribbble.com/search?q=nintendo)

## Outcome

- A `[first-name]-n64-voter.zip` file with the source code.
- It should not take you more than roughly 4 hours.
- We'd rather have you state you don't know something than delivering messy code.
- Quality `>` quantity. User experience `>` functions.